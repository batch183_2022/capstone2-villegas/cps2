const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a products
router.post("/", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}
	else{
		res.send("You don't have permission on this page!");
	}
});

// Route for retrieving all active products
router.get("/", (req, res) => {
	productControllers.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific product
router.get("/:productId", (req, res)=>{
	console.log(req.params.productId);
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
});


// Update product info(admin only)
router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
		productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		return res.send("Failed");
	}
})

// Archive a product
router.patch("/:productId/archive", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
		productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
	}
	else{
		return res.send("Failed!");
	}

})

module.exports = router;