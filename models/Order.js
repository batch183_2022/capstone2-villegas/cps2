const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
            type: String,
            required: [true, "User Id is required"]
    },
    productId: {
            type: String,
            required: [true, "Product Id is required"]
    },
    price: {
        type: Number, 
    },
    quantity: {
        type: Number, 
    },
    total: {
        type: Number,
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    status:     {
                type: String,
                default: "pending"
                }
            
})

module.exports = mongoose.model("Order", orderSchema);
