const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");

// User Registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body.
	2. Make sure that the password is encrypted.
	3. Save the new User to the database.

*/
module.exports.registerUser= (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result != null && result.email == reqBody.email){
			return "Duplicate email found!";
		}
		else{
			if(reqBody.email != "" && reqBody.password != ""){
				let newUser = new User({
					firstName: reqBody.firstName,
					lastName: reqBody.lastName,
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				});
				return newUser.save().then((user, error) =>{
						if(error){
							return false;
						}
						else{
							console.log(newUser);
							return true;
						}
					})
			}
			else{
				return "Both email and password must be provided!"; 
			}
		}
	})
}


// Retrieve all users
module.exports.retrieveAllUsers = () =>{
	return User.find({}).then(result => result);
}

//User login
/*
	Steps:
	1. Check the database if the user's email is registered.
	2. Compare the password provided in the login form with the password in the database.
	3. Generate and return a JSON web token if the user is successfully login and return false if not.
*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return "User does not exist";
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return "Password does not match!";
			}
		}
	})
}

//set a user to admin
module.exports.updateUserStatus = (userId) =>{

	let updatedStatus = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(userId, updatedStatus).then((updatedStatus, error) =>{
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})	
}

//set a user change status
module.exports.updateStatus = (userId) =>{

	let updateStatus = {
		isAdmin: false
	}
	return User.findByIdAndUpdate(userId, updateStatus).then((updatedStatus, error) =>{
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})	
}



//Create Order - Customer only
module.exports.createOrder = async (data) =>{
let newOrder 
	let isOrderUpdated = await Order.findById(data.userId).then(orderResult =>{
				
				newOrder = new Order({
					userId: data.userId,
					productId: data.productId,
					price: data.price,
					quantity: data.quantity,
					total: data.total
				})
				return newOrder.save().then((order, error) =>{
					if(error){
						return false;
						}
					else{
						return true;
					}	
				})
		})
	let isProductUpdated = await Product.findById(data.productId).then(product =>{
		product.stocks = product.stocks - data.quantity;
		return product.save().then((stocks, error) =>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isPriceUpdated = await Product.findById(newOrder.productId).then(priceResult =>{
		newOrder.price = priceResult.price;
		newOrder.total = priceResult.price * newOrder.quantity;
		return newOrder.save().then((order, error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	console.log(newOrder);
	if(isOrderUpdated && isProductUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}

}


// Retrieve User’s orders (Customer) 
module.exports.getUserOrders = (data) =>{
	return Order.find({data}).then(result => result);
}

// Retrieve all orders (Admin only)
module.exports.getAllOrders = () =>{
	return Order.find({}).then(result => result);
}


	


