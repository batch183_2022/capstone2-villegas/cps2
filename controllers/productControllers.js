const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})
	return newProduct.save().then((product, error)=>{

		if(error){
			return false;
		}

		else{
			return true;
		}

	})
}

// Retrieve all Active Products
/*
	Step:
	1. Retrieve all the products from the database
*/
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive:true}).then(result  => result);
}

// Retrieving a specific product
/*
	Step:
	1. Retrieve the product that matches the product ID provided from the URL.
*/
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}


// Update a product info
/*
	Step:
	1. create a variable "updatedProduct" which will contain the  information retrieved from the request body.
	2. find and update the product using the productID retrieved from request params/url property and the variable "updatedCourse" containing the information from the request body.
*/
module.exports.updateProduct = (productId, reqBody) =>{
	// Specify the fields / properties to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		isActive: reqBody.isActive
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (productId) => {
	let productArchived = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productId, productArchived).then((updatedStatus, err)=>{
		if(err){
			return false;
		}
		else{
			return true;
		}
	})
}

